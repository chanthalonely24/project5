
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import HomePage from "./modules/home";
import BlogPage from "./modules/blog";
import ContactUsPage from "./modules/contact";
import ErrorPage from "./modules/error/ErrorPage";
import Layout from "./modules/layout";
import "bootstrap/dist/css/bootstrap.min.css"
import "./assets/css/index.css"
import Login from "./modules/auth/components/Login";
import {useAuth} from "./modules/auth/components/Auth";
import ProductDetail from "./modules/product/components/ProductDetail";

const App = () => {


  const {auth} = useAuth();

  return (
    <BrowserRouter>
      <Routes>
        {auth ?
          <Route path="/" element={<Layout />}>
            <Route index element={<HomePage />} />
            <Route path="blog" element={<BlogPage />} />
            <Route path="contact" element={<ContactUsPage />} />
            {/* <Route path="product/:id" element={<ProductDetail />} /> */}
            <Route path="*" element={<Navigate to="/" />}></Route>
          </Route>
          :
          <>
            <Route index path="auth/login" element={<Login />} />
            <Route path="*" element={<Navigate to="/auth/login" />}></Route>
          </>
        }
        <Route path="error" element={<ErrorPage />} />

      </Routes>
    </BrowserRouter>
  );
};


export default App;
